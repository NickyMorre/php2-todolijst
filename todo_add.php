<?php

require_once('bootstrap.php');


if (isset($_POST['addtodo'])) {
    $query->insert('todos', [
        'titel' => $_POST['titel'],
        'omschrijving' => $_POST['omschrijving'],
        'datum' => $_POST['datum'],
        'status' => $_POST['status'] ?? 0,

    ]);
    header('Location: index.php');


}


?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voeg taken toe</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">
<div class="row">
    <div class="col-sm-12 center">
        <h2 class="titel">Voeg taak toe</h2>
        <form action="todo_add.php" method="post">
            <label class="add-titel" for="titel">Titel</label>
            <input class="form-control" value="" id="titel" type="text" name="titel">

            <label class="add-datum" for="datum">Datum</label>
            <input class="form-control" id="datum" type="date" name="datum">

            <label class="add-omschrijving" for="omschrijving">Omschrijving</label>
            <textarea class="form-control" id="omschrijving" name="omschrijving"></textarea><br>

            <input class="btn btn-success" type="submit" name="addtodo" value="Verzend">
        </form>
    </div>
</div>
</div>
</body>
</html>