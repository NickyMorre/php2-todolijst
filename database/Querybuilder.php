<?php
class Querybuilder{

    public $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function selectAll($table){
        $classname = substr(ucfirst($table),0,-1);

        $stmt = $this->pdo->prepare("SELECT * FROM $table");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,$classname);
    }

    public function selectwithid($table,$id){
        $classname = substr(ucfirst($table),0,-1);

        $stmt = $this->pdo->prepare("SELECT * FROM $table WHERE id=$id");
        $stmt->execute();


        //return $stmt->fetchAll(PDO::FETCH_CLASS,$classname)[0];

        $stmt->setFetchMode(PDO::FETCH_CLASS,$classname);
        return $stmt->fetch();
    }

    public function delete($table,$id){
        $stmt = $this->pdo->prepare("DELETE FROM $table WHERE id=$id");
        $stmt->execute();
    }

    public function insert($table,$parameters){
        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $table,
            implode(', ',array_keys($parameters)),
            ':'.implode(', :',array_keys($parameters))
        );

        try{
            $stmt = $this->pdo->prepare($sql);

            $stmt->execute($parameters);
        }
        catch(PDOException $e){
            die($e->getMessage());
        }


    }

    public function update($table,$parameters,$id){
        // UPDATE todos SET titel = :titel, omschrijving = :omschrijving, datum = :datum, status = :status WHERE id = 1

        foreach(array_keys($parameters) as $key){
            $setjes[] = $key.' = :'.$key;
        }

        $sql = sprintf(
            'UPDATE %s SET %s WHERE id=%s',
            $table,
            implode(', ',$setjes),
            $id
        );

        //dd($sql);

        try{
            $stmt = $this->pdo->prepare($sql);

            $stmt->execute($parameters);
        }
        catch(PDOException $e){
            die($e->getMessage());
        }
    }


    public function checkstatus($table, $column, $id)
    {
        $sql = sprintf("UPDATE %s SET {$column}= !{$column} WHERE id = %s",
            $table,
            $id);
        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();

        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

       //dd($sql);












}