<?php

require_once('bootstrap.php');


if(isset($_GET['id'])){
	$todo = $query->selectwithid('todos',$_GET['id']);
	//dd($taak);
}


if(isset($_POST['edittodo'])){
	$query->update('todos',[
		'titel' => $_POST['titel'],
		'omschrijving' => $_POST['omschrijving'],
		'datum' => $_POST['datum'],
		'status' => $_POST['status'] ?? 0,
	],$_POST['id']);

	header('Location: index.php');



	

}


?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Taken</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="titel">Pas taak aan</h2>
            <form action="todo_edit.php" method="post">
                <input type="hidden" name="id" value="<?= $todo->id;?>">

                <label class="add-titel" for="titel">Titel</label>
                <input class="form-control" value="<?= $todo->titel;?>" id="titel" type="text" name="titel">

                <label class="add-datum" for="datum">Datum</label>
                <input class="form-control" value="<?= $todo->datum;?>" id="datum" type="date" name="datum">

                <label class="add-omschrijving" for="omschrijving">Omschrijving</label>
                <textarea class="form-control" id="omschrijving" name="omschrijving"><?= $todo->omschrijving;?></textarea>

                <label class="add-status" for="status">Status</label>
                <span><p>Klaar? vink dan aan</p></span>
                <input class="form-control" value="1" id="status" type="checkbox" name="status" <?= $todo->status ? 'checked' : '' ;?>><br>
                <input class="btn btn-success" type="submit" name="edittodo" value="Verzend">

            </form>
        </div>
    </div>
</div>
</body>
</html>