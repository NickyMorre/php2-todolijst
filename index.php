<?php

require_once('bootstrap.php');


$todos = $query->selectAll('todos');

?>
    <!doctype html>
    <html lang="nl">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Overzicht taken</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="hoofdtitel">Takenlijst</h2>
                <a class="btn btn-primary" href="todo_add.php">Taken toevoegen</a>
            </div>
        </div>
    </div>
    <hr>
<?php
foreach ($todos as $todo) {
    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 center">
                <span class="afgerond <?= $todo->status ? 'klaar' : '' ?>"></span><br><br>
                <span class="datum <?= $todo->status ? 'done' : '' ?>"><?= date('d-m-Y', strtotime($todo->datum)); ?></span>
                <h2 class="titel <?= $todo->status ? 'done' : '' ?>"><?= $todo->titel; ?></h2>
                <span class="text <?= $todo->status ? 'done' : '' ?>"><?= $todo->omschrijving; ?></span><br><br>
                <span><a class="btn btn-success" href="todo_edit.php?id=<?= $todo->id; ?>">Bewerken</a></span>
                <span><a class="btn btn-danger" href="todo_delete.php?id=<?= $todo->id; ?>">Verwijderen</a></span>
                <span>
                    <a class="btn btn-warning"
                         href="todo_done.php?id=<?= $todo->id; ?>"><?= $todo->status ? 'Niet klaar' : 'Klaar' ?></a></span>
            </div>
        </div>
    </div>
    <hr>
    </body>
    </html>
    <?php
}



